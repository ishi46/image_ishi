import cv2
import numpy as np

face_cascade_path = '/home/ayato/Desktop/Ri-one/opencv-3.3.1/data/haarcascades/haarcascade_frontalface_default.xml'
eye_cascade_path = '/home/ayato/Desktop/Ri-one/opencv-3.3.1/data/haarcascades/haarcascade_eye.xml'

face_cascade = cv2.CascadeClassifier(face_cascade_path)
eye_cascade = cv2.CascadeClassifier(eye_cascade_path)

ORG_WINDOW_NAME = "cap"
GAUSSIAN_WINDOW_NAME = "cap_gray"

cap = cv2.VideoCapture(0)  #カメラ映像取得

end_flag, c_frame = cap.read()
height, width, channels = c_frame.shape

cv2.namedWindow(ORG_WINDOW_NAME)
cv2.namedWindow(GAUSSIAN_WINDOW_NAME)   #ウィンドウの準備


while end_flag == True:

    img = c_frame
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    face_list = face_cascade.detectMultiScale(img_gray, minSize=(100,100))

    for x, y, w, h in face_list:
        cv2.rectangle(img_gray, (x, y), (x + w, y + h), (255, 0, 0), 2)
        face = img[y: y + h, x: x + w]
        face_gray = img_gray[y: y + h, x: x + w]
        eyes = eye_cascade.detectMultiScale(face_gray)
        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(face, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

    cv2.imshow(ORG_WINDOW_NAME, c_frame)
    cv2.imshow(GAUSSIAN_WINDOW_NAME, img_gray) #フレーム表示

    k = cv2.waitKey(1)
    if k == 27:
        break

    end_flag, c_frame = cap.read() #次のフレーム読み込み
#終了処理
cv2.waitKey(0)
cv2.destroyAllWindows()
