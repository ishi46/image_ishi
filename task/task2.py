import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while(1):

    ret, frame = cap.read()
    gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    cv2.imshow('gray', gray)
    cv2.imshow('frame', frame)

    k = cv2.waitKey(1)
    if k == 27:
        break


cv2.destroyAllWindows()
